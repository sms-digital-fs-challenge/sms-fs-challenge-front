const { merge } = require("webpack-merge");
const commonConfig = require("./webpack.common.js");
const dotenv = require("dotenv");
dotenv.config();
const reactAppMode = process.env.REACT_APP_MODE;
const env = reactAppMode == "development" ? "dev" : "prod";

module.exports = () => {
  const envConfig = require(`./webpack.${env}.js`);
  const config = merge(commonConfig, envConfig);
  return config;
};
