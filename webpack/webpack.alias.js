const path = require("path");

module.exports = {
  "@": path.resolve(__dirname, "../src"),
  "@assets": path.resolve(__dirname, "../src/assets"),
  "@styles": path.resolve(__dirname, "../src/assets/styles"),
  "@images": path.resolve(__dirname, "../src/assets/images"),
  "@layouts": path.resolve(__dirname, "layouts"),
  "@pages": path.resolve(__dirname, "../src/pages"),
  "@routes": path.resolve(__dirname, "../src/routes"),
  "@store": path.resolve(__dirname, "../src/store"),
  "@theme": path.resolve(__dirname, "../src/theme"),
  "@utils": path.resolve(__dirname, "../src/utils"),
  "@helpers": path.resolve(__dirname, "../src/utils/helpers"),
};
