import React from "react";
import { useRoutes } from "react-router-dom";
import Landing from "@layouts/Landing";
import DataTable from "@pages/DataTable";
import { DASHBOARD, DATA, LANDING, NOTFOUND } from "@utils/helpers/paths";
import NotFound from "@pages/NotFound";
import Dashboard from "@layouts/Dashboard";

export default function Router() {
  return useRoutes([
    {
      path: LANDING,
      element: <Landing />,
    },
    { path: DASHBOARD, element: <Dashboard /> },
    { path: DATA, element: <DataTable /> },
    { path: NOTFOUND, element: <NotFound /> },
  ]);
}
