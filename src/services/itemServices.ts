import { IItem } from "@models/item.model";
import { GET_ITEMS_LIST } from "@utils/helpers/endpoints";
import { ApiError, ApiSuccess } from "@utils/types/axiosInterfaces";
import { genericGetAxios } from "../utils/api/genericApis";

export const getItems = (
  query: string,
  _callback: (error?: ApiError, response?: ApiSuccess<IItem[]>) => void
) => genericGetAxios(_callback, `${GET_ITEMS_LIST}?${query}`);
