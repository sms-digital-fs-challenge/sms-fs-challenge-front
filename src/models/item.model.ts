import { getFormattedDate } from "@utils/helpers/functions";

export interface IItem {
  _id?: string;
  id: number;
  city: string;
  startDate: string;
  endDate: string;
  status: string;
  price: number;
  color: string;
}

export const decodeItem = (requestResponse: any): IItem => {
  return {
    _id: requestResponse._id,
    id: requestResponse.id,
    city: requestResponse.city,
    startDate: getFormattedDate(requestResponse.startDate),
    endDate: getFormattedDate(requestResponse.endDate),
    status: requestResponse.status,
    price: requestResponse.price,
    color: requestResponse.color,
  };
};
