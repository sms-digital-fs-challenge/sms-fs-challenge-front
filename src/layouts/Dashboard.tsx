import Navbar from "@components/Navbar";
import { Stack } from "@mui/material";
import DataTable from "@pages/DataTable";
import React from "react";

function Dashboard() {
  return (
    <div style={{ height: "100vh" }}>
      <Navbar />
      <Stack direction="column" padding="5% 10%">
        <DataTable />
      </Stack>
    </div>
  );
}
export default Dashboard;
