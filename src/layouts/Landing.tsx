import { Button, Grow, Stack, styled } from "@mui/material";
import styledC from "styled-components";
import LoginIcon from "@mui/icons-material/Login";
import React from "react";
import { useNavigate } from "react-router-dom";
import { DASHBOARD } from "@utils/helpers/endpoints";
const CustomBox = styled(Stack)(({ theme }) => ({
  height: "100vh",
  justifyContent: "center",
  alignItems: "center",
  background:
    "linear-gradient(90deg, rgba(85,175,237,1) 0%, rgba(209,179,254,1) 100%)",
}));
const CustomH3 = styledC.h3`
  font-size: 40px;
  text-align: center;
`;

function Landing() {
  const navigate = useNavigate();
  function navigateToDashboard() {
    navigate(DASHBOARD);
  }
  return (
    <CustomBox spacing={2}>
      <Grow in={true}>
        <CustomH3 className="custom-font">
          Welcome to SMS Digital FS challenge. <br></br>Click the button to view
          data
        </CustomH3>
      </Grow>
      <Grow in={true}>
        <Button onClick={navigateToDashboard}>
          <LoginIcon className="custom-font" style={{ marginRight: "20px" }} />
          Enter website
        </Button>
      </Grow>
    </CustomBox>
  );
}

export default Landing;
