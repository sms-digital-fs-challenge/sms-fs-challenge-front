import { Stack, styled, TextField, Typography } from "@mui/material";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import moment from "moment";
import React, { useState } from "react";

const CustomTypography = styled(Typography)(() => ({
  marginRight: "20px",
}));
const CustomDesktopDatePicker = styled(DesktopDatePicker)(() => ({
  marginRight: "20px",
}));

interface IProps {
  setfields: (b: Object) => void;
  setitems: (b: Object) => void;
  fields: Object;
}

function DataTableHeader({ setfields, setitems, fields }: IProps) {
  const [startDate, setStartDate] = useState<Date | null>(null);
  const [endDate, setEndDate] = useState<Date | null>(null);
  function handleStartDateChange(newValue: Date | null) {
    setStartDate(newValue);
    setitems([]);
    setfields({
      ...fields,
      page: 1,
      refresh: Math.random(),
      startDate:
        newValue != null
          ? moment(newValue).format("YYYY-MM-DDTHH:mm:ss.SSSS[Z]")
          : undefined,
    });
  }
  function handleEndDateChange(newValue: Date | null) {
    setEndDate(newValue);
    setitems([]);
    setfields({
      ...fields,
      page: 1,
      refresh: Math.random(),
      endDate:
        newValue != null
          ? moment(newValue).format("YYYY-MM-DDTHH:mm:ss.SSSS[Z]")
          : undefined,
    });
  }
  return (
    <React.Fragment>
      <CustomTypography variant="h4">Items list</CustomTypography>
      <Stack
        direction="row"
        alignItems="stretch"
        style={{ marginBottom: "20px" }}
      >
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <CustomDesktopDatePicker
            label="Start Date"
            inputFormat="MM/dd/yyyy"
            value={startDate}
            onChange={handleStartDateChange}
            renderInput={(params: any) => <TextField {...params} />}
          />
          <CustomDesktopDatePicker
            label="End Date"
            inputFormat="MM/dd/yyyy"
            value={endDate}
            onChange={handleEndDateChange}
            renderInput={(params: any) => <TextField {...params} />}
          />
        </LocalizationProvider>
      </Stack>
    </React.Fragment>
  );
}

export default DataTableHeader;
