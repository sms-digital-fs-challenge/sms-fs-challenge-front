import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import ArrowDown from "@mui/icons-material/ArrowDownward";
import ArrowUp from "@mui/icons-material/ArrowUpward";
import SortIcon from "@mui/icons-material/Sort";
import { Grow } from "@mui/material";
import { Box } from "@mui/system";
import { useState, useEffect } from "react";
import { ApiError, ApiSuccess } from "@utils/types/axiosInterfaces";
import { decodeItem, IItem } from "@models/item.model";
import { getItems } from "@services/itemServices";
import styled from "styled-components";
import DataTableHeader from "@layouts/section/DataTableHeader";
const Label = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
`;

interface Column {
  range: number;
  id: "city" | "startDate" | "endDate" | "price" | "status" | "color";
  label: string;
  minWidth?: number;
  align?: "right";
  format?: (value: number) => string;
}

const columns: readonly Column[] = [
  { range: 0, id: "city", label: "City", minWidth: 100 },
  { range: 1, id: "startDate", label: "Start\u00a0Date", minWidth: 170 },
  { range: 2, id: "endDate", label: "End\u00a0Date", minWidth: 170 },
  {
    range: 3,
    id: "price",
    label: "Price",
    minWidth: 100,
    format: (value: number) => value.toLocaleString("en-US"),
  },
  {
    range: 4,
    id: "status",
    label: "Status",
    minWidth: 100,
    align: "right",
    format: (value: number) => value.toLocaleString("en-US"),
  },
  {
    range: 5,
    id: "color",
    label: "Color",
    minWidth: 100,
    align: "right",
    format: (value: number) => value.toFixed(2),
  },
];

export default function DataTable() {
  interface IFields {
    page: number;
    limit: number;
    sort: any;
    refresh: number | undefined;
    startDate?: Date;
    endDate?: Date;
  }
  const [fields, setFields] = useState<IFields>(() => {
    return {
      page: 1,
      limit: 10,
      sort: "-city",
      refresh: undefined,
    };
  });
  const defaultFields = fields;

  const [items, setItems] = useState<any>([]);

  const [page, setPage] = useState(0);
  // const [hasMore, setHasMore] = useState(false);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [showArrowSort, setShowArrowSort] = useState(11);
  const [totalRows, setTotalRows] = useState(0);

  const [sortToggle, setSortToggle] = useState({
    range: -1,
    field: "",
    operator: "+",
  });
  function handleCellSort(sortObject: any) {
    setItems([]);
    setFields({
      ...fields,
      sort: `${sortObject.operator}${sortObject.field}`,
    });
  }
  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
    setItems([]);
    setFields({ ...fields, page: newPage + 1 });
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
    setItems([]);
    setFields({ ...fields, limit: parseInt(event.target.value, 10) });
  };
  function generateQuery(fields: IFields) {
    let query = "";
    if (fields.page) query = query.concat(`page=${fields.page}&`);
    if (fields.limit) query = query.concat(`limit=${fields.limit}&`);
    if (fields.sort) query = query.concat(`sort=${fields.sort}&`);
    if (fields.startDate)
      query = query.concat(`startDate=${fields.startDate}&`);
    if (fields.endDate) query = query.concat(`endDate=${fields.endDate}&`);
    return query.slice(0, query.length - 1);
  }
  useEffect(() => {
    setLoading(true);
    setError(false);

    async function exec() {
      let callbackResponse = (
        error?: ApiError,
        response?: ApiSuccess<IItem[]>
      ) => {
        if (response) {
          let data = response.data.data
            ? response.data.data.map(decodeItem)
            : [];
          if (data && !(data.length > 0)) {
            setLoading(false);
            // return;
          }
          setItems([...items, ...data]);
        }
        if (error) {
          // TODO: ADD Toast error
          console.error("An error occured ", error);
        }
      };
      await getItems(generateQuery(fields), callbackResponse);
      let callbackResponseOther = (
        error?: ApiError,
        response?: ApiSuccess<IItem[]>
      ) => {
        if (response) {
          let data = response.data.data ?? [];
          if (data && data.length > 0) {
            setTotalRows(fields.page * fields.limit + data.length);
          } else {
            setTotalRows(
              (fields.page == 1 ? fields.page : fields.page - 1) *
                fields.limit +
                data?.length
            );
          }
        }
        if (error) {
          // TODO: ADD Toast error
          console.error("An error occured");
        }
      };
      await getItems(
        generateQuery({ ...fields, page: fields.page + 1 }),
        callbackResponseOther
      );

      setLoading(false);
    }
    exec();
  }, [fields]);

  return (
    <React.Fragment>
      <DataTableHeader
        setfields={setFields}
        setitems={setItems}
        fields={fields}
      />
      <Grow in={true}>
        <TableContainer>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                    onMouseEnter={() => {
                      setShowArrowSort(column.range);
                    }}
                    onMouseLeave={() => {
                      setShowArrowSort(11);
                    }}
                    onClick={() => {
                      setShowArrowSort(11);
                      let operator = "+";
                      let inverseOperator =
                        sortToggle.operator === "-" ? "+" : "-";
                      if (sortToggle.range === column.range)
                        operator = inverseOperator;

                      setSortToggle({
                        range: column.range,
                        operator: operator,
                        field: column.id,
                      });
                      handleCellSort({
                        range: column.range,
                        operator: operator,
                        field: column.id,
                      });
                    }}
                  >
                    <Label
                      style={{
                        position: "relative",
                        float: [4, 5].includes(column.range) ? "right" : "none",
                      }}
                    >
                      <SortIcon
                        style={{
                          fontSize: "15px",
                          position: "absolute",
                          left: "-20px",
                          color: "gray",
                          display:
                            showArrowSort === column.range ? "inherit" : "none",
                        }}
                      />

                      {sortToggle.range === column.range &&
                      sortToggle.operator === "-" ? (
                        <ArrowDown
                          style={{
                            fontSize: "15px",
                            display:
                              sortToggle.range === column.range
                                ? "inherit"
                                : "none",
                          }}
                        />
                      ) : (
                        <ArrowUp
                          style={{
                            fontSize: "15px",
                            display:
                              sortToggle.range === column.range
                                ? "inherit"
                                : "none",
                          }}
                        />
                      )}
                      {column.label}
                    </Label>
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {items.map((row: IItem) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                    {columns.map((column) => {
                      const value = row[column.id];
                      return (
                        <TableCell key={column.id} align={column.align}>
                          {column.format && typeof value === "number" ? (
                            column.format(value)
                          ) : column.id === "color" ? (
                            <Box
                              sx={{
                                width: 30,
                                height: 30,
                                borderRadius: 50,
                                backgroundColor: value,
                                float: "right",
                              }}
                            />
                          ) : (
                            value
                          )}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Grow>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={totalRows}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </React.Fragment>
  );
}
