import SimpleButton from "@components/commons/SimpleButton";
import SimpleSpan from "@components/commons/SimpleSpan";
import { HOMEPAGE } from "@utils/helpers/endpoints";
import React from "react";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";

const Container = styled.div`
  width: 100vw;
  display: flex;
  align-items: flex-start;
  justify-content: space-around;
`;
const TextContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const Title = styled.h1`
  font-size: 70px;
  cursor: pointer;
`;

function NotFound() {
  const navigate = useNavigate();
  function navigateToHome(): void {
    navigate(HOMEPAGE);
  }

  return (
    <div
      style={{
        display: "grid",
        placeItems: "center",
        width: "100vw",
        height: "100vh",
      }}
    >
      <Container>
        <TextContainer>
          <Title className="custom-font">Oops</Title>
          <SimpleSpan
            color="gray"
            text={
              "The page you are looking for does not exist or may have been deleted"
            }
            fontSize="1.1rem"
            fontWeight="300"
            margin="0"
            pointer
          />
          <SimpleButton
            variant="contained"
            text="Go back to home"
            onClick={navigateToHome}
            margin="40px 0 0 0"
            width="300px"
            style={{ backgroundColor: "#47b6e8" }}
          />
        </TextContainer>
      </Container>
    </div>
  );
}

export default NotFound;
