import { AxiosError, AxiosResponse } from "axios";

export type ApiError = AxiosError<ApiErrorResponse>;

interface ApiErrorResponse {
  errorCode: number;
  status: string;
  result: boolean;
  message: string;
  data?: any;
}

export type ApiSuccess<T = any> = AxiosResponse<ApiSuccessResponse<T>>;
interface ApiSuccessResponse<T> {
  status: string;
  result: boolean;
  message: string;
  accessToken: string;
  loggedUser: T;
  rows?: number;
  data?: T;
  new?: T;
  old?: T;
}

export type ApiCallback<T = any> = (
  error?: ApiError,
  response?: ApiSuccess<T>
) => void;
