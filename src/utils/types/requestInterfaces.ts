export interface IResponse {
  status?: string;
  statusCode?: number;
  result?: boolean;
  message?: string;
  errorCode?: number;
  rows?: number;
  data?: any;
}
