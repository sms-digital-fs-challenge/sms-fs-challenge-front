import axios from "axios";
const baseUrl: string | undefined = process.env.REACT_APP_BASE_URL;

const api = axios.create({
  baseURL: baseUrl,
});

export default api;
