import { IItem } from "@models/item.model";
import { Method } from "axios";
import { ApiError, ApiSuccess } from "../types/axiosInterfaces";
import api from "./index";

export async function fetchAxiosApi<T>(
  url: string,
  method: Method,
  data = {},
  options = {},
  _callback: (error?: ApiError, response?: ApiSuccess<T>) => void
) {
  let response: ApiSuccess;
  let headers: any;
  try {
    headers = {
      Accept: "application/json",
      "Content-Type": "application/json",
    };

    const responseBody = await api.request<any, ApiSuccess<IItem[]>>({
      method,
      headers,
      url: `${url}`,
      data,
      timeout: 90000,
      ...options,
    });
    response = responseBody;
    _callback(undefined, response);
  } catch (err: any) {
    _callback(err, undefined);
  }
}
