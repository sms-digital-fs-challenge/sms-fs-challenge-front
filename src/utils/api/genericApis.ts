import { ApiError, ApiSuccess } from "@utils/types/axiosInterfaces";
import { IResponse } from "@utils/types/requestInterfaces";
import { fetchAxiosApi } from "./request";

export const genericGetAxios = async (
  _callback: (
    error?: ApiError | undefined,
    response?: ApiSuccess | undefined
  ) => void,
  url: string
): Promise<IResponse> => {
  let result: IResponse | undefined = {};
  await fetchAxiosApi(`${url}`, "GET", {}, {}, _callback);
  return result;
};
