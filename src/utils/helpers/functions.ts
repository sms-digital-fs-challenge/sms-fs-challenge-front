import moment from "moment";

export function getFormattedDate(date: string) {
  return moment(date).format("MM/DD/YYYY");
}
