export const LANDING = "/";
export const DATA = "/data";
export const DASHBOARD = "/dashboard";
export const NOTFOUND = "*";
