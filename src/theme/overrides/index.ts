import { Theme } from "@mui/material";
import Button from "./Button";
import Tooltip from "./Tooltip";
import Table from "./Table";

// ----------------------------------------------------------------------

export default function ComponentsOverrides(theme: Theme) {
  return Object.assign({
    ...Button(theme),
    ...Tooltip(theme),
    ...Table(theme),
  });
}
