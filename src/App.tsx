import Router from "@routes";
import React from "react";

import ThemeProvider from "./theme";
function App() {
  return (
    <ThemeProvider>
      <Router />
    </ThemeProvider>
  );
}

export default App;
