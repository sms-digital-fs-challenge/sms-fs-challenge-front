import { Button, Stack, styled } from "@mui/material";
import { HOMEPAGE } from "@utils/helpers/endpoints";
import React from "react";
import { useNavigate } from "react-router-dom";
import SmsDigitalLogo from "../assets/images/sms-digital-logo.png";
import styledC from "styled-components";

const CustomNavBar = styled(Stack)(({ theme }) => ({
  backgroundColor: theme.palette.primary.light,
  height: "10%",
  boxShadow: theme.shadows[18],
  color: "white",
}));

const Logo = styledC.img`
    width: 10%;
    height: 57%;
    border-radius: 10px;
`;

function Navbar() {
  const navigate = useNavigate();
  function navigateToHome(): void {
    navigate(HOMEPAGE);
  }
  return (
    <CustomNavBar
      direction="row"
      padding="0 5%"
      justifyContent="space-between"
      alignItems="center"
    >
      <a target="_blank" rel="noreferrer" href="https://sms-digital.com">
        <Logo src={SmsDigitalLogo} alt="SMS Digital" />
      </a>

      <Button variant="contained" color="warning" onClick={navigateToHome}>
        Back to home
      </Button>
    </CustomNavBar>
  );
}

export default Navbar;
