import React from "react";

interface ISpanParams {
  fontSize: string;
  color: string;
  text: any;
  fontWeight?: string;
  margin?: string;
  padding?: string;
  pointer?: boolean;
  underline?: boolean;
  floatRight?: boolean;
  style?: any;
  onClick?: () => void;
}
function SimpleSpan(props: ISpanParams) {
  return (
    <span
      className="custom-font"
      onClick={props.onClick}
      style={{
        fontSize: props.fontSize ? props.fontSize : "12px",
        color: props.color ? props.color : "black",
        margin: props.margin ? props.margin : "0",
        padding: props.padding ? props.padding : "0",
        fontWeight: props.fontWeight ? props.fontWeight : "0",
        cursor: props.pointer ? "pointer" : "",
        textDecoration: props.underline ? "underline" : "",
        ...props.style,
      }}
    >
      {props.text}
    </span>
  );
}

export default SimpleSpan;
