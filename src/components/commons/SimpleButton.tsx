import Button from "@mui/material/Button";
import React from "react";

interface IButtonParams {
  text: any;
  margin?: string;
  variant: "text" | "outlined" | "contained";
  disabled?: boolean;
  width?: string;
  height?: string;
  icon?: any;
  onClick?: any;
  onMouseEnter?: any;
  onMouseLeave?: any;
  color?:
    | "inherit"
    | "primary"
    | "secondary"
    | "success"
    | "error"
    | "info"
    | "warning"
    | undefined;
  floatRight?: boolean;
  style?: {};
}
const SimpleButton = (props: IButtonParams) => {
  return (
    <Button
      className="custom-font"
      variant={props.variant ? props.variant : "contained"}
      color={props.color ? props.color : "primary"}
      disabled={props.disabled ? props.disabled : false}
      onClick={props.onClick}
      onMouseEnter={props.onMouseEnter}
      onMouseLeave={props.onMouseLeave}
      style={{
        margin: props.margin !== undefined ? props.margin : "0",
        width: props.width !== undefined ? props.width : "",
        height: props.height && props.height,
        float: props.floatRight && props.floatRight ? "right" : "left",
        ...props.style,
      }}
    >
      <div
        style={{
          display: "flex",
          alignItems: "center",
        }}
      >
        {props.icon}
        {props.text}
      </div>
    </Button>
  );
};

export default SimpleButton;
