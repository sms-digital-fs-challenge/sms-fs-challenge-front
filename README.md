# Project Title

SMS Digital Challenge for Fullstack position
Front part

## Authors

- [@Houcem Eddine SANAI](https://tn.linkedin.com/in/houssem-eddine-sanai)

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`REACT_APP_MODE` to be {development | production }

`REACT_APP_BASE_URL` to set the backend URI

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/sms-digital-fs-challenge/sms-fs-challenge-front
```

Go to the project directory

```bash
  cd sms-fs-challenge-front
```

Install dependencies

```bash
  yarn
```

Start the server

```bash
  yarn start
```

## Demo

Demo available here https://smsdigital.amitechss.com

## Feedback

If you have any feedback, please reach out to us at sanai.houcem@gmail.com

## License

[MIT](https://choosealicense.com/licenses/mit/)
